class RPNCalculator

  def initialize
    @stack = []
  end

  def plus
    err
    sum = self.pop + self.pop
    self.push(sum)
  end

  def minus
    err
    first_num = self.pop
    sec_num = self.pop
    self.push(sec_num - first_num)
  end

  def times
    err
    prod = self.pop * self.pop
    self.push(prod)
  end

  def divide
    err
    first_num = self.pop.to_f
    sec_num = self.pop.to_f
    self.push(sec_num / first_num)
  end

  def value
    @stack.last
  end

  def push(thing)
    @stack.push(thing)
  end

  def pop
    @stack.pop
  end

  def size
    @stack.length
  end

  def tokens(input)
    token = input.split
    token.map do |el|
      if is_int?(el)
        el.to_i
      else
        el.to_sym
      end
    end
  end

  def is_int?(str)
    str == str.to_i.to_s
  end

  def evaluate(str)
    self.tokens(str).each do |el|
      if el.is_a? Integer
        self.push(el)
      else
        self.plus if el == :+
        self.minus if el == :-
        self.times if el == :*
        self.divide if el == :/
      end
    end
    self.value
  end

  def err
    raise "calculator is empty" if self.size < 2
  end
end
